#!/usr/bin/env node

import args from './args';
import * as commands from './commands';

Promise.resolve(args).then(async (args) => {
    for (const command of args._) switch (command) {
        case "update":
            await commands[command](args);
            break;

        default:
            throw new Error(`Unknown command '${command}'`);
    }
}).catch((err) => {
    console.error((err instanceof Error) ? `${err.name}: ${err.message}` : `${err}`);
    process.exit(1);
});
