/**
 * @see https://api.webglobe.com/
 */

import fetch from "node-fetch";
import { URL } from "url";
import * as z from "zod";

const BASE_URL = "https://api.webglobe.com/";

export class WeglobeError extends Error {
    public static fromResponseData(body: unknown): WeglobeError {
        let message: string | undefined, code: number | undefined;

        if ((typeof body === "object") && body) {
            // @ts-expect-error
            if (("message" in body) && (typeof body.message === "string")) message = body.message;
            // @ts-expect-error
            if (("code" in body) && (typeof body.code === "number")) code = body.code;
            // @ts-expect-error
            if ((message === undefined) && (code === undefined) && ("error" in body)) return WeglobeError.fromResponseData(body.error);
        }

        return new WeglobeError(
            message ?? "Unknown error",
            code ?? 0
        );
    }

    public constructor (message: string, public readonly code: number) {
        super(message);
    }
}

async function req<T = unknown>(opts: {
    endpoint: string;
    method?: "GET" | "POST" | "PUT" | "DELETE";
    body?: Record<string, unknown>;
    headers?: Record<string, string>;
}): Promise<{ status: number; statusText: string; body: unknown }> {
    const res = await fetch(new URL(opts.endpoint, BASE_URL), {
        headers: {
            ...((opts.body === undefined) ? {} : { "Content-Type": "application/json" }),
            ...(opts.headers ?? {}),
        },
        body: ((opts.body === undefined) ? undefined : JSON.stringify(opts.body)),
        method: opts.method ?? "GET",
    });

    console.debug(`WEGLOBE-REQ: ${opts.method ?? "GET"} ${opts.endpoint} - ${res.status} ${res.statusText}`)
    
    return {
        status: res.status,
        statusText: res.statusText,
        body: await res.json()
    };


}

const loginRes = z.object({
    success: z.literal(true),
    data: z.object({
        token: z.string(),
    }),
});

export async function login(params: { login: string; password: string; otp?: string; sms?: string; }): Promise<z.infer<typeof loginRes>["data"]> {
    const { status, body } = await req({
        endpoint: "auth/login",
        method: "POST",
        body: params,
    });

    if (status === 200) return loginRes.parse(body).data;

    throw WeglobeError.fromResponseData(body);
}

const logoutRes = z.object({
    success: z.literal(true),
});
export async function logout(params: { token: string }): Promise<void> {
    const { status, body } = await req({
        endpoint: "auth/logout",
        headers: { "Authorization": "Bearer " + params.token }
    });

    if (status === 200) {
        logoutRes.parse(body);
        return;
    }

    throw WeglobeError.fromResponseData(body);
}

export namespace domains {
    const listRes = z.object({
        success: z.literal(true),
        all: z.array(z.object({
            domain: z.string(),
            domain_id: z.number(),
            object_type: z.literal('DOMAIN'),
        })),
    });
    export async function list(params: { token: string }): Promise<z.infer<typeof listRes>["all"]> {
        const { status, body } = await req({
            endpoint: "domains",
            headers: { "Authorization": "Bearer " + params.token },
        });

        if (status === 200) return listRes.parse(body).all;

        throw WeglobeError.fromResponseData(body);
    }
}

export namespace dns {
    const listRes = z.object({
        success: z.literal(true),
        data: z.object({
            soa: z.object({
                id: z.number(),
                origin: z.string(),
                ns: z.string(),
                mbox: z.string(),
                serial: z.number(),
                refresh: z.number(),
                retry: z.number(),
                expire: z.number(),
                minimum: z.number(),
                ttl: z.number(),
                changed: z.number(),
                deleted: z.number(),
                new: z.number(),
                suspend: z.number(),
                error: z.number(),
                error_message: z.string(),
                updated_at: z.string(),
            }),
            records_types: z.array(z.string()),
            enabled_rr_types: z.array(z.string()),
            records: z.array(z.object({
                id: z.number(),
                zone: z.number(),
                name: z.string(),
                type: z.union([
                    z.literal("A"),
                    z.literal("AAAA"),
                    z.literal("ALIAS"),
                    z.literal("CAA"),
                    z.literal("CNAME"),
                    z.literal("HINFO"),
                    z.literal("MX"),
                    z.literal("NAPTR"),
                    z.literal("NS"),
                    z.literal("PTR"),
                    z.literal("RP"),
                    z.literal("SRV"),
                    z.literal("SSHFP"),
                    z.literal("TLSA"),
                    z.literal("TXT"),
                ]),
                data: z.string(),
                aux: z.number(),
                ttl: z.number(),
                edata: z.string(),
                edatakey: z.string().nullable(),
            })),
            export: z.string(),
        }),
    });
    export async function list(params: { token: string, domain_id: number }): Promise<z.infer<typeof listRes>["data"]> {
        const { status, body } = await req({
            endpoint: params.domain_id + "/dns",
            headers: { "Authorization": "Bearer " + params.token },
        });

        if (status === 200) return listRes.parse(body).data;

        throw WeglobeError.fromResponseData(body);
    }

    const createRes = z.object({
        success: z.literal(true),
    });
    export async function create(params: {
        token: string;
        domain_id: number;
        name: string;
        ttl: number;
        type: "A" | "AAAA" | "MX" | "TXT" | "CNAME" | "SRV";
        aux?: number;
        data: string;
    }): Promise<void> {
        const { status, body } = await req({
            method: "POST",
            endpoint: params.domain_id + "/dns",
            headers: { "Authorization": "Bearer " + params.token },
            body: {
                name: params.name,
                ttl: params.ttl,
                type: params.type,
                aux: params.aux,
                data: params.data,
            }
        });

        if (status === 200) {
            createRes.parse(body);
            return;
        }

        throw WeglobeError.fromResponseData(body);
    }

    const updateRes = z.object({
        success: z.literal(true),
    });
    export async function update(params: {
        token: string;
        domain_id: number;
        dns_id: number;
        name: string;
        ttl: number;
        type: "A" | "AAAA" | "MX" | "TXT" | "CNAME" | "SRV";
        aux?: string;
        data: string;
    }): Promise<void> {
        const { status, body } = await req({
            method: "PUT",
            endpoint: params.domain_id + "/dns/" + params.dns_id,
            headers: { "Authorization": "Bearer " + params.token },
            body: {
                name: params.name,
                ttl: params.ttl,
                type: params.type,
                aux: params.aux,
                data: params.data,
            }
        });

        if (status === 200) {
            updateRes.parse(body);
            return;
        }

        throw WeglobeError.fromResponseData(body);
    }

    const removeRes = z.object({
        success: z.literal(true),
    });
    export async function remove(params: {
        token: string;
        domain_id: number;
        dns_id: number;
    }): Promise<void> {
        const { status, body } = await req({
            method: "DELETE",
            endpoint: params.domain_id + "/dns/" + params.dns_id,
            headers: { "Authorization": "Bearer " + params.token },
        });

        if (status === 200) {
            removeRes.parse(body);
            return;
        }

        throw WeglobeError.fromResponseData(body);
    }
}