import * as weglobe from '../weglobe-api';
import os, { NetworkInterfaceInfo } from 'os';
import fetch from 'node-fetch';
import https from 'https';

type GeneralDnsRecord = { type: "A" | "AAAA"; data: string };
const MASK10 = 0b1111111111000000;

async function getExtenralIPv4(): Promise<string | undefined> {
    try {
        const res = await fetch("https://ifconfig.me/ip", {
            agent: new https.Agent({
                family: 4,
            })
        });
        if (res.ok) {
            const extIp = (await res.text()).trim();
            if (/^[1,2]?[0-9]{1,2}\.[1,2]?[0-9]{1,2}\.[1,2]?[0-9]{1,2}\.[1,2]?[0-9]{1,2}$/.test(extIp)) return extIp;
        }
    } catch (err: unknown) {
        console.warn("Failed to get external IPv4", err);
    }
}

enum FamilyType {
    IPv6, IPv4, Other
};

function getFamily(family: NetworkInterfaceInfo['family']): FamilyType {
    switch (family) {
        case 'IPv4': // Node < v18
        // @ts-expect-error
        case 4: // Node >= v18
            return FamilyType.IPv4;
        case 'IPv6':
        // @ts-expect-error
        case 6:
            return FamilyType.IPv6;
    }

    return FamilyType.Other;
}

async function getRecords(): Promise<GeneralDnsRecord[]> {
    const ifaces = os.networkInterfaces();

    const dns: GeneralDnsRecord[] = [];
    for (const iface of Object.values(ifaces)) {
        if (iface) for (const info of iface) {
            if (info.internal) continue;

            if (getFamily(info.family) === FamilyType.IPv6) {
                const block10 = parseInt(info.address.split(":")[0], 16) & MASK10;
                if (block10 === (0xfe80 & MASK10)) continue; // Local-link

                dns.push({
                    type: "AAAA",
                    data: info.address,
                });
            }
        }
    }

    const extIPv4 = await getExtenralIPv4();
    if (extIPv4) {
        dns.push({
            type: "A",
            data: extIPv4,
        });
    }

    return dns;
}

export default async function(options: {
    hostname: string;
    login: string;
    password: string;
    ttl: number;
    wildcard: boolean;
}): Promise<void> {
    const hostname = options.hostname.toLowerCase();
    const domain = hostname.split(".").slice(-2).join(".");
    const subdomain = hostname.split(".").slice(0, -2).join(".");

    const newDnsRecords: Array<{
        data: string;
        name: string;
        type: "A" | "AAAA" | "CNAME";
        ttl: number;
    }> = (await getRecords()).map((rec) => ({
        ...rec,
        name: subdomain,
        ttl: options.ttl
    }));

    if (options.wildcard) {
        newDnsRecords.push({ type: "CNAME", data: `${hostname}.`, name: `*.${subdomain}`, ttl: options.ttl });
    }

    console.debug(`Will set subdomain='${subdomain}' of domain='${domain}': \n${newDnsRecords.map((nR) => JSON.stringify(nR))}`);

    const { token } = await weglobe.login({ login: options.login, password: options.password });
    try {
        const domains = await weglobe.domains.list({ token });
        const { domain_id } = domains.find((d) => d.domain === domain) ?? {};
        if (typeof domain_id === "undefined") throw new Error(`Unable to access domain '${domain}'. Availible domains: ${domains.map((d) => d.domain).join(", ")}`);

        const names = new Set([subdomain, subdomain ? `*.${subdomain}` : `*`]);
        const currentDnsRecords = await weglobe.dns.list({ token, domain_id });
        for (const cR of currentDnsRecords.records) {
            if (names.has(cR.name.toLowerCase())) {
                await weglobe.dns.remove({ token, domain_id, dns_id: cR.id });
            }
        }
        for (const newR of newDnsRecords) {
            await weglobe.dns.create({
                token,
                domain_id,
                ...newR,
            });
        }
    } finally {
        weglobe.logout({ token });
    }
}
