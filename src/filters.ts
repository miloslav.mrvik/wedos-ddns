export function notNull<T = unknown>(x: null | T): x is T {
    return x !== null;
}

export function notUndefined<T = unknown>(x: undefined | T): x is T {
    return typeof x !== "undefined";
}