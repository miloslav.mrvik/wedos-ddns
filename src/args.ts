import yargs from "yargs/yargs";
import { hideBin } from "yargs/helpers";
import * as os from "os";

const args = yargs(hideBin(process.argv))
    .option("login", {
        alias: "l",
        type: "string",
        demandOption: true,
        describe: "API username",
        global: true,
    })
    .option("password", {
        alias: "p",
        type: "string",
        demandOption: true,
        describe: "API password",
        global: true,
    })
    .command("update", "update DNS to current situation", (yargs) => {
        return yargs
            .option("hostname", {
                alias: "h",
                type: "string",
                describe: "Hostname to use.",
                demandOption: true,
            })
            .options("ttl", {
                type: "number",
                default: 300,
                describe: "TTL for the DNS record"
            })
            .options("wildcard", {
                type: "boolean",
                default: false,
                describe: "Add *.subdomain.domain.tld CNAME"
            })
    })
    .demandCommand(1, 1, "please supply exactly one command", "please supply exactly one command")
    .completion()
    .argv;

export default args;