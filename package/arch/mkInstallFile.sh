#!/usr/bin/env bash

cat <<EOF
#!/usr/bin/env bash

# script is run right before files are extracted. One argument is passed: new package version.
#function pre_install() {
#    echo pre_install;
#}

# script is run right after files are extracted. One argument is passed: new package version.
function post_install() {
    if ! [[ -L "${npm_package_prefix}/bin/${npm_package_name}" ]]; then
        ln -s "${npm_package_prefix}/share/${npm_package_name}/${npm_package_main}" "${npm_package_prefix}/bin/${npm_package_name}";
    fi;
}

# script is run right before files are extracted. Two arguments are passed in the following order: new package version, old package version.
#function pre_upgrade() {
#    echo pre_upgrade;
#}

# script is run after files are extracted. Two arguments are passed in the following order: new package version, old package version.
#function post_upgrade() {
#    echo post_upgrade;
#}

# script is run right before files are removed. One argument is passed: old package version.
#function pre_remove() {
#    echo pre_remove;
#}

# script is run right after files are removed. One argument is passed: old package version.
function post_remove() {
    if [[ -L "${npm_package_prefix}/bin/${npm_package_name}" ]]; then
        rm "${npm_package_prefix}/bin/${npm_package_name}";
    fi;
}

EOF
